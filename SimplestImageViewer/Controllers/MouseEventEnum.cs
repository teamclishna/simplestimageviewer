﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimplestImageViewer.Controllers
{
    /// <summary>
    /// メインウィンドウ上でのマウス操作を示す列挙子
    /// </summary>
    [Flags]
    public enum MouseEventEnum
    {
        // クリック位置に関する列挙子
        ClickLeftSide = 0x0001,
        ClickRightSide = 0x0002,
        ClickUpSide = 0x0004,
        ClickDownSide = 0x0008,
        ClickCenter = 0x0010,
        // クリックしたボタンに関する列挙子
        ClickLeftButton = 0x0100,
        ClickRightButton = 0x0200,
        // ホイール操作
        WheelUp = 0x1000,
        WheelDown = 0x2000,
    }
}
