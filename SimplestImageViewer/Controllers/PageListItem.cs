﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimplestImageViewer.Controllers
{
    /// <summary>
    /// ページ一覧のコンボボックスに表示するアイテム
    /// </summary>
    public class PageListItem
    {
        /// <summary>
        /// このアイテムに対応するコントローラ上に格納している画像のインデックス
        /// </summary>
        public int ImageIndex { get; set; }

        /// <summary>
        /// コンボボックスに表示するテキスト
        /// </summary>
        public string Text { get; set; }
    }
}
