﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace SimplestImageViewer.Controllers
{
    /// <summary>
    /// アプリケーションが開いた画像を格納するオブジェクト
    /// </summary>
    public class OpenedImage
    {
        /// <summary>
        /// 開く画像ファイル
        /// </summary>
        public FileInfo Path { get; set; }

        /// <summary>
        /// 画像ファイルを開いた画像イメージ.
        /// ディレクトリ単位で開いた場合、ページ移動で該当のファイルを最初に選択した時点でロードする.
        /// それまではnullのままにしておく.
        /// </summary>
        public BitmapImage OpenImage { get; set; }
    }
}
