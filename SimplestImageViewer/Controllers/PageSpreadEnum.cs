﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace SimplestImageViewer.Controllers
{
    /// <summary>
    /// 見開き表示の方法を定義する列挙子
    /// </summary>
    [DataContract(Name = "PageSpreadEnum")]
    public enum PageSpreadEnum
    {
        /// <summary>1ページずつ表示</summary>
        Single,
        /// <summary>見開き表示で左→右</summary>
        SpreadLtR,
        /// <summary>見開き表示で右→左</summary>
        SpreadRtL,
    }
}
