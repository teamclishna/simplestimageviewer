﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SimplestImageViewer.Utils;
using SimplestImageViewer.VM;
using SimplestImageViewer.Windows;

using clUtils.log;

namespace SimplestImageViewer.Controllers
{
    public partial class AppController
    {
        /// <summary>
        /// 指定したページへ移動する
        /// </summary>
        /// <param name="pageIndex"></param>
        public void MovePage(
            int pageIndex,
            MainWindow wnd)
        {
            this.Logger.Print(LogLevel.DEBUG, $"Controller#MovePage: pageIndex = {pageIndex}");

            var vm = wnd.DataContext as MainWindowViewModel;

            // 画像を表示していない場合は無視
            if (vm.PageListItems.Count == 0)
            { return; }

            // 表示対象のページの範囲外が指定された場合は補正する
            var viewIndex = (
                pageIndex < 0 ? 0 :
                vm.PageListItems.Count <= pageIndex ? vm.PageListItems.Count - 1 :
                pageIndex);

            if (vm.PageSpread == PageSpreadEnum.Single)
            {
                // 単一ページ表示
                this.Logger.Print(LogLevel.DEBUG, $"Controller#MovePage: Show page = {vm.PageListItems[viewIndex].ImageIndex}");

                var img = vm.Images[vm.PageListItems[viewIndex].ImageIndex];
                ActivateImage(img);
                vm.ViewImage1 = img;
                vm.ViewImage2 = null;
            }
            else
            {
                // 見開き表示

                this.Logger.Print(LogLevel.DEBUG, $"Controller#MovePage: Show page 1 = {vm.PageListItems[viewIndex].ImageIndex}");
                var img = vm.Images[vm.PageListItems[viewIndex].ImageIndex];
                ActivateImage(img);
                vm.ViewImage1 = img;

                if (viewIndex == 0 && this.Config.FirstPageForceSingle)
                {
                    // 先頭ページの表示、かつ先頭ページの1ページ表示強制の場合は画像2を表示しない
                    this.Logger.Print(LogLevel.DEBUG, $"Controller#MovePage: Not show page 2.");
                    vm.ViewImage2 = null;
                }
                else if (vm.PageListItems[viewIndex].ImageIndex < vm.Images.Count - 1)
                {
                    // 見開きの2ページ目として次のページを表示
                    this.Logger.Print(LogLevel.DEBUG, $"Controller#MovePage: Show page 2 = {vm.PageListItems[viewIndex].ImageIndex + 1}");
                    var img2 = vm.Images[vm.PageListItems[viewIndex].ImageIndex + 1];
                    ActivateImage(img2);
                    vm.ViewImage2 = img2;
                }
                else
                {
                    // 最終ページ
                    this.Logger.Print(LogLevel.DEBUG, $"Controller#MovePage: Not show page 2.");
                    vm.ViewImage2 = null;
                }
            }

            wnd.MainViewer.ScrollToHorizontalOffset(0.0);
            wnd.MainViewer.ScrollToVerticalOffset(0.0);
        }

        /// <summary>
        /// 指定した画像オブジェクトの実体をロードする
        /// </summary>
        /// <param name="img"></param>
        /// <returns></returns>
        private bool ActivateImage(OpenedImage img)
        {
            // ロード済みである場合は何もしない
            if(img.OpenImage != null)
            { return false; }
            else
            {
                img.OpenImage = ImagingUtils.LoadImage(img.Path.FullName);
                return true;
            }
        }

        /// <summary>
        /// メインウィンドウのViewModelを取得して返す
        /// </summary>
        /// <param name="wnd"></param>
        /// <returns></returns>
        private MainWindowViewModel GetViewModel(MainWindow wnd)
        { return wnd.DataContext as MainWindowViewModel; }

    }
}
