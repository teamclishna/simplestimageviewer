﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

using SimplestImageViewer.Utils;
using SimplestImageViewer.VM;
using SimplestImageViewer.Windows;

using clUtils.log;

namespace SimplestImageViewer.Controllers
{
    /// <summary>
    /// アプリケーションの動作を担当するコントローラ
    /// </summary>
    public partial class AppController
    {
        /// <summary>
        /// アプリケーションの設定情報を保存するパス
        /// </summary>
        public static readonly string CONF_PATH = "./TeamClishnA/SimplestImageViewer/Config";
        /// <summary>
        /// アプリケーションの設定情報のJSONファイル名
        /// </summary>
        public static readonly string CONF_FILE_NAME = "simplestimageviewer.json";

        /// <summary>
        /// 画像表示時の倍率
        /// </summary>
        public static readonly int[] ZoomList = new int[] {
            12,
            25,
            33,
            50,
            67,
            75,
            100,
            200,
            300,
            400,
        };

        /// <summary>
        /// ロガーオブジェクト
        /// </summary>
        public ILogger Logger { get; set; }

        /// <summary>
        /// アプリケーションの設定情報
        /// </summary>
        public AppConfig Config { get; set; }

        /// <summary>
        /// アプリケーション起動時に渡されたコマンドライン引数
        /// </summary>
        public string[] LaunchArgs { get; set; }

        /// <summary>
        /// ファイル、またはフォルダをドロップした時の処理
        /// </summary>
        /// <param name="wnd"></param>
        /// <param name="droppedPath">読み込み対象の画像ファイル、またはディレクトリのパス</param>
        /// <remarks>
        /// 現在はスレッドでロードする OpenFileAsync を使う
        /// </remarks>
        public void OpenFile(
            MainWindow wnd,
            string droppedPath)
        {
            this.Logger.PrintPush(LogLevel.DEBUG, $"Open File. {droppedPath}");

            var images = new List<OpenedImage>();
            var droppedDir = new DirectoryInfo(droppedPath);
            if (droppedDir.Exists)
            {
                this.Logger.Print(LogLevel.DEBUG, $"dropped object is directory.");

                // ディレクトリをドロップした場合、ディレクトリ内の画像を開く
                var imageFiles = new List<OpenedImage>();
                foreach (var f in droppedDir.GetFiles())
                {
                    if (ImagingUtils.IsImageFile(f))
                    {
                        imageFiles.Add(new OpenedImage()
                        {
                            Path = f,
                            OpenImage = null,
                        });
                    }
                }

                this.Logger.Print(LogLevel.DEBUG, $"directory has {imageFiles.Count} files.");

                // Listに格納したファイルをファイル名順にソートする
                images = imageFiles
                    .OrderBy(x => x.Path.Extension)
                    .OrderBy(x => x.Path.Name)
                    .ToList();
            }
            else
            {
                this.Logger.Print(LogLevel.DEBUG, $"dropped object is file.");

                if (ImagingUtils.IsImageFile(new FileInfo(droppedPath)))
                {
                    var opened = new OpenedImage()
                    {
                        Path = new FileInfo(droppedPath),
                        OpenImage = null,
                    };
                    images = new List<OpenedImage>() { opened };
                }
                else
                { images = new List<OpenedImage>(); }
            }

            // ページ一覧を生成する
            var vm = wnd.DataContext as MainWindowViewModel;
            var pagelist = CreatePageList(images, vm.PageSpread);

            // 読み込んだ画像一覧、ページ一覧をViewModelに渡し、有効な画像が存在するのであれば
            // 1ページ目を表示する
            vm.Images = images;
            vm.PageListItems = pagelist;
            vm.PageIndex = 0;

            this.Logger.PrintPop();
            return;
        }

        /// <summary>
        /// ファイル、またはフォルダをドロップした時の処理 (バックグラウンド読み込み)
        /// </summary>
        /// <param name="wnd"></param>
        /// <param name="droppedPath">読み込み対象の画像ファイル、またはディレクトリのパス</param>
        /// <remarks>
        public void OpenFileAsync(
            MainWindow wnd,
            string droppedPath)
        {
            new System.Threading.Thread(() => {
                this.Logger.PrintPush(LogLevel.DEBUG, $"(Worker Thread) Open File Async. {droppedPath}");

                var images = new List<OpenedImage>();
                var droppedDir = new DirectoryInfo(droppedPath);

                if (droppedDir.Exists)
                {
                    this.Logger.Print(LogLevel.DEBUG, $"(Worker Thread) dropped object is directory.");

                    // ディレクトリをドロップした場合、ディレクトリ内の画像を開く
                    var imageFiles = new List<OpenedImage>();
                    int loadedFileIndex = 0;
                    foreach (var f in droppedDir.GetFiles())
                    {
                        if (ImagingUtils.IsImageFile(f))
                        {
                            imageFiles.Add(new OpenedImage()
                            {
                                Path = f,
                                OpenImage = null,
                            });

                            this.Logger.Print(LogLevel.DEBUG, $"(Worker Thread) Loaded { f.Name }");
                            loadedFileIndex++;
                            wnd.Dispatcher.Invoke((Action)(() => {
                                // ファイルロードの進行状況を表示
                                var vm = wnd.DataContext as MainWindowViewModel;
                                vm.LoadingProgress = new MainWindowViewModel.FileLoadingProgress()
                                {
                                    Files = droppedDir.GetFiles().Length,
                                    Current = loadedFileIndex,
                                    LastLoaded = f.Name,
                                };
                                wnd.UpdateViewModel(vm);
                            }));
                        }
                    }

                    this.Logger.Print(LogLevel.DEBUG, $"(Worker Thread) directory has {imageFiles.Count} files.");

                    // Listに格納したファイルをファイル名順にソートする
                    images = imageFiles
                        .OrderBy(x => x.Path.Extension)
                        .OrderBy(x => x.Path.Name)
                        .ToList();
                }
                else
                {
                    this.Logger.Print(LogLevel.DEBUG, $"(Worker Thread) dropped object is file.");

                    if (ImagingUtils.IsImageFile(new FileInfo(droppedPath)))
                    {
                        var opened = new OpenedImage()
                        {
                            Path = new FileInfo(droppedPath),
                            OpenImage = null,
                        };
                        images = new List<OpenedImage>() { opened };
                    }
                    else
                    { images = new List<OpenedImage>(); }
                }

                // ページ一覧を生成する
                wnd.Dispatcher.Invoke((Action)(() => {
                    var vm = wnd.DataContext as MainWindowViewModel;
                    var pagelist = CreatePageList(images, vm.PageSpread);

                    // 読み込んだ画像一覧、ページ一覧をViewModelに渡し、有効な画像が存在するのであれば
                    // 1ページ目を表示する
                    vm.Images = images;
                    vm.PageListItems = pagelist;
                    vm.PageIndex = 0;

                    // フォルダを開いた場合、見開きモードの自動切り替えが設定されている場合はここで変更する
                    if (droppedDir.Exists && this.Config.SetSpreadWhenFolder)
                    { vm.PageSpread = this.Config.PageSpreadWhenFolder; }

                    wnd.UpdateViewModel(vm);
                }));

                this.Logger.PrintPop();

            }).Start();
        }

        /// <summary>
        /// 指定した画像一覧を開くためのページリストを作成して返す
        /// </summary>
        /// <param name="images"></param>
        /// <param name="spread"></param>
        /// <returns></returns>
        public List<PageListItem> CreatePageList(
            List<OpenedImage> images,
            PageSpreadEnum spread)
        {
            var result = new List<PageListItem>();

            // 画像未表示の場合は何もせず空のページ情報を返す
            if (images.Count == 0)
            { return result; }

            if (spread == PageSpreadEnum.Single)
            {
                // 単一ページ表示の場合は画像とページが 1:1
                for (var index = 0; index < images.Count; index++)
                {
                    result.Add(new PageListItem()
                    {
                        ImageIndex = index,
                        Text = images[index].Path.Name,
                    });
                }
            }
            else
            {
                // 見開き表示の場合
                var index = 0;

                // 1ページ目は単一ページ強制？
                if (this.Config.FirstPageForceSingle)
                {
                    result.Add(new PageListItem()
                    {
                        ImageIndex = index,
                        Text = images[index].Path.Name,
                    });
                    index++;
                }
                while (index <= images.Count - 1)
                {
                    // 見開き可能？
                    if (index < images.Count - 1)
                    {
                        result.Add(new PageListItem()
                        {
                            ImageIndex = index,
                            Text = (
                                spread == PageSpreadEnum.SpreadLtR ? $"{images[index].Path.Name} - {images[index + 1].Path.Name}" :
                                $"{images[index + 1].Path.Name} - {images[index].Path.Name}"),
                        });
                        index += 2;
                    }
                    else
                    {
                        result.Add(new PageListItem()
                        {
                            ImageIndex = index,
                            Text = images[index].Path.Name,
                        });
                        index++;
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// メインウィンドウ上での操作をイベントとして渡す
        /// </summary>
        /// <param name="e"></param>
        /// <param name="wnd"></param>
        public void MouseAction(
            MouseEventEnum e,
            MainWindow wnd)
        {
            var vm = wnd.DataContext as MainWindowViewModel;

            // 最初にクリック位置によるページ遷移、ズームの判定を行う

            // ページ移動の判定
            if (this.Config.TransitionMethod == PageTransitionMethod.Side)
            {
                var pageTransIndex = 0;
                if (e.HasFlag(MouseEventEnum.ClickLeftSide))
                {
                    switch (vm.PageSpread)
                    {
                        case PageSpreadEnum.Single:
                            pageTransIndex = (this.Config.TransitionSideSingle == PageTransitionSide.Left ? 1 : -1);
                            break;
                        case PageSpreadEnum.SpreadLtR:
                            pageTransIndex = (this.Config.TransitionSideLtR == PageTransitionSide.Left ? 1 : -1);
                            break;
                        case PageSpreadEnum.SpreadRtL:
                            pageTransIndex = (this.Config.TransitionSideRtL == PageTransitionSide.Left ? 1 : -1);
                            break;
                    }
                }
                else if (e.HasFlag(MouseEventEnum.ClickRightSide))
                {
                    switch (vm.PageSpread)
                    {
                        case PageSpreadEnum.Single:
                            pageTransIndex = (this.Config.TransitionSideSingle == PageTransitionSide.Right ? 1 : -1);
                            break;
                        case PageSpreadEnum.SpreadLtR:
                            pageTransIndex = (this.Config.TransitionSideLtR == PageTransitionSide.Right ? 1 : -1);
                            break;
                        case PageSpreadEnum.SpreadRtL:
                            pageTransIndex = (this.Config.TransitionSideRtL == PageTransitionSide.Right ? 1 : -1);
                            break;
                    }
                }

                if (pageTransIndex == 1)
                {
                    if (vm.NextImageCommand.CanExecute(null))
                    { vm.NextImageCommand.Execute(null); }
                    return;
                }
                else if (pageTransIndex == -1)
                {
                    if (vm.PrevImageCommand.CanExecute(null))
                    { vm.PrevImageCommand.Execute(null); }
                    return;
                }
            }

            // ズームの設定
            if (this.Config.ZoomMethod == ImageZoomMethod.Side)
            {
                var zoomIndex = 0;
                if (e.HasFlag(MouseEventEnum.ClickUpSide))
                { zoomIndex = (!this.Config.InvertZoomIn ? -1 : 1); }
                else if (e.HasFlag(MouseEventEnum.ClickDownSide))
                { zoomIndex = (this.Config.InvertZoomIn ? -1 : 1); }

                if (zoomIndex == 1)
                {
                    if (vm.ZoomInCommand.CanExecute(null))
                    { vm.ZoomInCommand.Execute(null); }
                    return;
                }
                else if (zoomIndex == -1)
                {
                    if (vm.ZoomOutCommand.CanExecute(null))
                    { vm.ZoomOutCommand.Execute(null); }
                    return;
                }
            }

            // クリックのボタン、ホイールによる判定を行う

            // ページ遷移
            if (this.Config.TransitionMethod == PageTransitionMethod.Button)
            {
                var pageTransIndex = 0;
                if (e.HasFlag(MouseEventEnum.ClickLeftButton))
                {
                    switch (vm.PageSpread)
                    {
                        case PageSpreadEnum.Single:
                            pageTransIndex = (this.Config.TransitionSideSingle == PageTransitionSide.Left ? 1 : -1);
                            break;
                        case PageSpreadEnum.SpreadLtR:
                            pageTransIndex = (this.Config.TransitionSideLtR == PageTransitionSide.Left ? 1 : -1);
                            break;
                        case PageSpreadEnum.SpreadRtL:
                            pageTransIndex = (this.Config.TransitionSideRtL == PageTransitionSide.Left ? 1 : -1);
                            break;
                    }
                }
                else if (e.HasFlag(MouseEventEnum.ClickRightButton))
                {
                    switch (vm.PageSpread)
                    {
                        case PageSpreadEnum.Single:
                            pageTransIndex = (this.Config.TransitionSideSingle == PageTransitionSide.Right ? 1 : -1);
                            break;
                        case PageSpreadEnum.SpreadLtR:
                            pageTransIndex = (this.Config.TransitionSideLtR == PageTransitionSide.Right ? 1 : -1);
                            break;
                        case PageSpreadEnum.SpreadRtL:
                            pageTransIndex = (this.Config.TransitionSideRtL == PageTransitionSide.Right ? 1 : -1);
                            break;
                    }
                }

                if (pageTransIndex == 1)
                {
                    if (vm.NextImageCommand.CanExecute(null))
                    { vm.NextImageCommand.Execute(null); }
                    return;
                }
                else if (pageTransIndex == -1)
                {
                    if (vm.PrevImageCommand.CanExecute(null))
                    { vm.PrevImageCommand.Execute(null); }
                    return;
                }
            }

            // ズームの設定
            if (this.Config.ZoomMethod == ImageZoomMethod.MouseWheel)
            {
                var zoomIndex = 0;
                if (e.HasFlag(MouseEventEnum.WheelUp))
                { zoomIndex = (!this.Config.InvertZoomIn ? -1 : 1); }
                else if (e.HasFlag(MouseEventEnum.WheelDown))
                { zoomIndex = (this.Config.InvertZoomIn ? -1 : 1); }

                if (zoomIndex == 1)
                {
                    if (vm.ZoomInCommand.CanExecute(null))
                    { vm.ZoomInCommand.Execute(null); }
                    return;
                }
                else if (zoomIndex == -1)
                {
                    if (vm.ZoomOutCommand.CanExecute(null))
                    { vm.ZoomOutCommand.Execute(null); }
                    return;
                }
            }
        }

        /// <summary>
        /// メインウィンドウでのキー操作のイベント
        /// </summary>
        /// <param name="k"></param>
        /// <param name="wnd"></param>
        /// <returns>イベント処理を実行した場合はtrue.</returns>
        public bool KeyAction(
            Key k,
            MainWindow wnd)
        {
            var vm = wnd.DataContext as MainWindowViewModel;

            switch (k)
            {
                case Key.Enter:
                    if(vm.NextImageCommand.CanExecute(null))
                    { vm.NextImageCommand.Execute(null); }
                    break;
                case Key.Back:
                    if (vm.PrevImageCommand.CanExecute(null))
                    { vm.PrevImageCommand.Execute(null); }
                    break;
                case Key.Left:
                    {
                        var conf = ((App)App.Current).Controller.Config;
                        switch (vm.PageSpread)
                        {
                            case PageSpreadEnum.Single:
                                if(conf.TransitionSideSingle == PageTransitionSide.Left)
                                {
                                    if (vm.NextImageCommand.CanExecute(null))
                                    { vm.NextImageCommand.Execute(null); }
                                }
                                else
                                {
                                    if (vm.PrevImageCommand.CanExecute(null))
                                    { vm.PrevImageCommand.Execute(null); }
                                }
                                break;
                            case PageSpreadEnum.SpreadLtR:
                                if (conf.TransitionSideLtR == PageTransitionSide.Left)
                                {
                                    if (vm.NextImageCommand.CanExecute(null))
                                    { vm.NextImageCommand.Execute(null); }
                                }
                                else
                                {
                                    if (vm.PrevImageCommand.CanExecute(null))
                                    { vm.PrevImageCommand.Execute(null); }
                                }
                                break;
                            case PageSpreadEnum.SpreadRtL:
                                if (conf.TransitionSideRtL == PageTransitionSide.Left)
                                {
                                    if (vm.NextImageCommand.CanExecute(null))
                                    { vm.NextImageCommand.Execute(null); }
                                }
                                else
                                {
                                    if (vm.PrevImageCommand.CanExecute(null))
                                    { vm.PrevImageCommand.Execute(null); }
                                }
                                break;
                        }
                    }
                    break;
                case Key.Right:
                    {
                        var conf = ((App)App.Current).Controller.Config;
                        switch (vm.PageSpread)
                        {
                            case PageSpreadEnum.Single:
                                if (conf.TransitionSideSingle == PageTransitionSide.Right)
                                {
                                    if (vm.NextImageCommand.CanExecute(null))
                                    { vm.NextImageCommand.Execute(null); }
                                }
                                else
                                {
                                    if (vm.PrevImageCommand.CanExecute(null))
                                    { vm.PrevImageCommand.Execute(null); }
                                }
                                break;
                            case PageSpreadEnum.SpreadLtR:
                                if (conf.TransitionSideLtR == PageTransitionSide.Right)
                                {
                                    if (vm.NextImageCommand.CanExecute(null))
                                    { vm.NextImageCommand.Execute(null); }
                                }
                                else
                                {
                                    if (vm.PrevImageCommand.CanExecute(null))
                                    { vm.PrevImageCommand.Execute(null); }
                                }
                                break;
                            case PageSpreadEnum.SpreadRtL:
                                if (conf.TransitionSideRtL == PageTransitionSide.Right)
                                {
                                    if (vm.NextImageCommand.CanExecute(null))
                                    { vm.NextImageCommand.Execute(null); }
                                }
                                else
                                {
                                    if (vm.PrevImageCommand.CanExecute(null))
                                    { vm.PrevImageCommand.Execute(null); }
                                }
                                break;
                        }
                    }
                    break;
                case Key.S:
                    if (vm.SinglePageCommand.CanExecute(null))
                    { vm.SinglePageCommand.Execute(null); }
                    break;
                case Key.L:
                    if (vm.SpreadLtRCommand.CanExecute(null))
                    { vm.SpreadLtRCommand.Execute(null); }
                    break;
                case Key.R:
                    if (vm.SpreadRtLCommand.CanExecute(null))
                    { vm.SpreadRtLCommand.Execute(null); }
                    break;
                case Key.PageDown:
                    if (vm.ZoomInCommand.CanExecute(null))
                    { vm.ZoomInCommand.Execute(null); }
                    break;
                case Key.PageUp:
                    if (vm.ZoomOutCommand.CanExecute(null))
                    { vm.ZoomOutCommand.Execute(null); }
                    break;

                default:
                    // 定義されていないキーが入力された場合はfalseを返してイベントのハンドル未処理とする
                    return false;
            }

            return true;
        }

        /// <summary>
        /// アプリケーションの初期化処理
        /// </summary>
        /// <returns>初期化したこのオブジェクト</returns>
        public AppController Init()
        {
            // 設定情報をロードする
            var confDir = GetConfDir(false);
            var confPath = Path.Combine(confDir.FullName, AppController.CONF_FILE_NAME);
            try
            {
                using (var reader = new BinaryReader(new FileStream(confPath, FileMode.Open)))
                {
                    var fi = new FileInfo(confPath);
                    var bytes = reader.ReadBytes((int)fi.Length);
                    var jsonReader = new Utf8JsonReader(bytes);
                    this.Config = JsonSerializer.Deserialize<AppConfig>(ref jsonReader);
                }
            }
            catch (Exception eUnknown)
            {
                // ロードに失敗した場合は初期状態の設定を生成する
                this.Logger.Print(LogLevel.ERROR, $"Load config file failed. {eUnknown.Message}");
                this.Config = new AppConfig();
            }

            return this;
        }

        /// <summary>
        /// アプリケーションの設定を保存する
        /// </summary>
        /// <param name="conf"></param>
        public void SaveConfig(AppConfig conf)
        {
            var confDir = GetConfDir(true);
            var confPath = Path.Combine(confDir.FullName, AppController.CONF_FILE_NAME);

            using (var writer = new BinaryWriter(new FileStream(confPath, FileMode.Create)))
            {
                var jsonBytes = JsonSerializer.SerializeToUtf8Bytes(conf, new JsonSerializerOptions() { WriteIndented = true, });
                writer.Write(jsonBytes);
            }
        }

        /// <summary>
        /// 設定ファイルを保存するためのディレクトリを取得する.
        /// </summary>
        /// <param name="createIfNoeExists">ディレクトリが存在しない場合に作成する場合はtrue</param>
        /// <returns>設定ファイルの保存ディレクトリのパス情報</returns>
        private DirectoryInfo GetConfDir(bool createIfNoeExists)
        {
            var confDirPath = Path.Combine(
                Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                AppController.CONF_PATH);
            var confDir = new DirectoryInfo(confDirPath);

            if (!confDir.Exists && createIfNoeExists)
            { confDir.Create(); }

            return confDir;
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public AppController()
        { }
    }
}
