﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using clUtils.gui;
using clUtils.log;

using SimplestImageViewer.Controllers;
using SimplestImageViewer.Utils;
using SimplestImageViewer.Windows;

namespace SimplestImageViewer.VM
{
    /// <summary>
    /// メイン画面の ViewModel
    /// </summary>
    public partial class MainWindowViewModel : NotifiableViewModel
    {
        /// <summary>
        /// ディレクトリ単位で画像を開くときにその進行状況を格納するためのクラス
        /// </summary>
        public class FileLoadingProgress
        {
            /// <summary>読み込み対象の画像の総数</summary>
            public int Files { get; set; }

            /// <summary>Filesの中で読み込み完了したファイルの個数</summary>
            public int Current { get; set; }

            /// <summary>直前に読み込み完了したファイル名</summary>
            public string LastLoaded { get; set; }
        }

        #region Fields

        private OpenedImage _img1;
        private OpenedImage _img2;

        private PageSpreadEnum _pageSpread;

        private int _zoomIndex;
        private int[] _zoomList;

        private List<PageListItem> _pageListItems;
        private int _pageIndex;

        private FileLoadingProgress _loadingProgress;

        #endregion

        #region Properties

        /// <summary>
        /// ロガーオブジェクト
        /// </summary>
        public ILogger Logger { get; set; }

        /// <summary>
        /// このViewModelをバインディングするウィンドウ
        /// </summary>
        public MainWindow Owner { get; set; }

        /// <summary>
        /// このViewModelで開く画像の一覧
        /// </summary>
        public List<OpenedImage> Images { get; set; }

        /// <summary>
        /// このViewModelで開くページの一覧.
        /// 1ページ表示の場合はImagesと 1:1 に対応する PageListItem が格納される.
        /// 見開き表示の場合は2ページごとに1つの PageListItem が格納される.
        /// また、このプロパティをセットした時点で読み込み状況を示すオブジェクトを破棄する.
        /// </summary>
        public List<PageListItem> PageListItems
        {
            get { return this._pageListItems; }
            set
            {
                this.Logger.Print(LogLevel.DEBUG, $"Set PageListItems (has {value.Count} items.)");

                this._pageListItems = value;
                this._loadingProgress = null;

                OnPropertyChanged(nameof(PageListItems));
                OnPropertyChanged(nameof(LoadingProgress));
                OnPropertyChanged(nameof(TitleLabel));
            }
        }

        /// <summary>
        /// 現在開いているページのインデックス.
        /// </summary>
        public int PageIndex
        {
            get { return this._pageIndex; }
            set
            {
                this.Logger.Print(LogLevel.DEBUG, $"Set PageIndex {value}");
                this._pageIndex = (value < 0 ? 0 : value);

                var controller = ((App)App.Current).Controller;
                controller.MovePage(this._pageIndex, this.Owner);

                OnPropertyChanged(nameof(PageIndex));

                this.PrevImageCommand.RaiseCanExecuteChanged();
                this.NextImageCommand.RaiseCanExecuteChanged();
                this.FirstImageCommand.RaiseCanExecuteChanged();
                this.LastImageCommand.RaiseCanExecuteChanged();
            }
        }

        /// <summary>
        /// 選択可能な表示倍率の配列
        /// </summary>
        public int[] ZoomList
        {
            get { return this._zoomList; }
            set
            {
                this._zoomList = value;
                OnPropertyChanged(nameof(ZoomList));
                OnPropertyChanged(nameof(ZoomLabelList));
            }
        }

        /// <summary>
        /// コンボボックスに表示する倍率表示
        /// </summary>
        public string[] ZoomLabelList
        {
            get
            {
                // 数値として格納している拡大率をパーセント表示の文字列に変換してから返す
                return this
                    ._zoomList
                    .Select(x => $"{x}%")
                    .ToArray();
            }
        }

        public OpenedImage ViewImage1
        {
            get { return this._img1; }
            set
            {
                this._img1 = value;
                OnPropertyChanged(nameof(ViewImage1));
                OnPropertyChanged(nameof(ViewImage1Image));
                OnPropertyChanged(nameof(ViewImage1Visibility));
                OnPropertyChanged(nameof(ViewImage1Left));
                OnPropertyChanged(nameof(ViewImage1Top));
                OnPropertyChanged(nameof(ViewImage1Width));
                OnPropertyChanged(nameof(ViewImage1Height));
                OnPropertyChanged(nameof(ViewImage2Visibility));
                OnPropertyChanged(nameof(ViewImage2Left));
                OnPropertyChanged(nameof(ViewImage2Top));
                OnPropertyChanged(nameof(ViewPortLeft));
                OnPropertyChanged(nameof(ViewPortTop));
                OnPropertyChanged(nameof(ViewPortWidth));
                OnPropertyChanged(nameof(ViewPortHeight));
                OnPropertyChanged(nameof(TitleLabel));

                // コマンドの操作可否を変更
                // 基本的に1無しで2だけ変更することはありえないのでここでだけチェックすればよい
                this.PrevImageCommand.RaiseCanExecuteChanged();
                this.NextImageCommand.RaiseCanExecuteChanged();
                this.FirstImageCommand.RaiseCanExecuteChanged();
                this.LastImageCommand.RaiseCanExecuteChanged();

                this.SinglePageCommand.RaiseCanExecuteChanged();
                this.SpreadLtRCommand.RaiseCanExecuteChanged();
                this.SpreadRtLCommand.RaiseCanExecuteChanged();
            }
        }

        public BitmapImage ViewImage1Image
        { get { return (this._img1 == null ? null : this._img1.OpenImage); } }

        public double ViewImage1Left
        {
            get
            {
                return (
                    this.PageSpread == PageSpreadEnum.Single ? 0.0 :
                    this.PageSpread == PageSpreadEnum.SpreadLtR ? 0.0 :
                    this._img2.CalcWidth(this._zoomList[this._zoomIndex]));
            }
        }

        public double ViewImage1Top
        { get { return ImagingUtils.CalcTop(this._img1, this._img2, this._zoomList[this._zoomIndex]); } }

        public double ViewImage1Width
        { get { return this._img1.CalcWidth(this._zoomList[this._zoomIndex]); } }

        public double ViewImage1Height
        { get { return this._img1.CalcHeight(this._zoomList[this._zoomIndex]); } }

        public Visibility ViewImage1Visibility
        { get { return (this._img1 != null ? Visibility.Visible : Visibility.Collapsed); } }

        public OpenedImage ViewImage2
        {
            get { return this._img2; }
            set
            {
                this._img2 = value;
                OnPropertyChanged(nameof(ViewImage2Visibility));
                OnPropertyChanged(nameof(ViewImage2));
                OnPropertyChanged(nameof(ViewImage2Image));
                OnPropertyChanged(nameof(ViewImage2Left));
                OnPropertyChanged(nameof(ViewImage2Top));
                OnPropertyChanged(nameof(ViewImage2Width));
                OnPropertyChanged(nameof(ViewImage2Height));
                OnPropertyChanged(nameof(ViewImage1Visibility));
                OnPropertyChanged(nameof(ViewImage1Left));
                OnPropertyChanged(nameof(ViewImage1Top));
                OnPropertyChanged(nameof(ViewPortLeft));
                OnPropertyChanged(nameof(ViewPortTop));
                OnPropertyChanged(nameof(ViewPortWidth));
                OnPropertyChanged(nameof(ViewPortHeight));
                OnPropertyChanged(nameof(TitleLabel));
            }
        }

        public BitmapImage ViewImage2Image
        { get { return (this._img2 == null ? null : this._img2.OpenImage); } }

        public double ViewImage2Left
        {
            get
            {
                return (
                    this.PageSpread == PageSpreadEnum.Single ? 0.0 :
                    this.PageSpread == PageSpreadEnum.SpreadLtR ? this._img1.CalcWidth(this._zoomList[this._zoomIndex]) :
                    0.0);
            }
        }

        public double ViewImage2Top
        { get { return ImagingUtils.CalcTop(this._img2, this._img1, this._zoomList[this._zoomIndex]); } }

        public double ViewImage2Width
        { get { return this._img2.CalcWidth(this._zoomList[this._zoomIndex]); } }

        public double ViewImage2Height
        { get { return this._img2.CalcHeight(this._zoomList[this._zoomIndex]); } }

        public Visibility ViewImage2Visibility
        { get { return (this._img2 != null ? Visibility.Visible : Visibility.Collapsed); } }

        public double ViewPortLeft
        { get { return 0.0; } }

        public double ViewPortTop
        { get { return 0.0; } }

        public double ViewPortWidth
        {
            get
            {
                if (this.PageSpread == PageSpreadEnum.Single)
                { return this._img1.CalcWidth(this._zoomList[this._zoomIndex]); }
                else
                { return this._img1.CalcWidth(this._zoomList[this._zoomIndex]) + this._img2.CalcWidth(this._zoomList[this._zoomIndex]); }
            }
        }

        public double ViewPortHeight
        { get { return this._img1.CalcHeight(this._zoomList[this._zoomIndex]); } }

        public PageSpreadEnum PageSpread
        {
            get { return this._pageSpread; }
            set
            {
                this.Logger.PrintPush(LogLevel.DEBUG, $"Set PageSpread: {value}");

                this._pageSpread = value;

                // ページ一覧を再生成する
                var controller = ((App)App.Current).Controller;
                this.PageListItems = controller.CreatePageList(this.Images, value);
                this.PageIndex = 0;

                this.Logger.PrintPop();

                OnPropertyChanged(nameof(IsSinglePage));
                OnPropertyChanged(nameof(IsSpreadRtL));
                OnPropertyChanged(nameof(IsSpreadLtR));
            }
        }

        public bool IsSinglePage
        { get { return this._pageSpread == PageSpreadEnum.Single; } }

        public bool IsSpreadRtL
        { get { return this._pageSpread == PageSpreadEnum.SpreadRtL; } }
        public bool IsSpreadLtR
        { get { return this._pageSpread == PageSpreadEnum.SpreadLtR; } }

        /// <summary>
        /// 画像表示時の倍率 (コンボボックスによって選択する時のインデックス)
        /// </summary>
        public int ZoomIndex
        {
            get { return this._zoomIndex; }
            set
            {
                this._zoomIndex = value;

                OnPropertyChanged(nameof(ZoomIndex));
                OnPropertyChanged(nameof(ViewImage1Left));
                OnPropertyChanged(nameof(ViewImage1Top));
                OnPropertyChanged(nameof(ViewImage1Width));
                OnPropertyChanged(nameof(ViewImage1Height));
                OnPropertyChanged(nameof(ViewImage2Left));
                OnPropertyChanged(nameof(ViewImage2Top));
                OnPropertyChanged(nameof(ViewImage2Width));
                OnPropertyChanged(nameof(ViewImage2Height));
                OnPropertyChanged(nameof(ViewPortLeft));
                OnPropertyChanged(nameof(ViewPortTop));
                OnPropertyChanged(nameof(ViewPortWidth));
                OnPropertyChanged(nameof(ViewPortHeight));
                OnPropertyChanged(nameof(TitleLabel));

                this.ZoomInCommand.RaiseCanExecuteChanged();
                this.ZoomOutCommand.RaiseCanExecuteChanged();
            }
        }

        /// <summary>
        /// ワーカースレッドでファイルを読み込み中の場合、その進捗状態を示すプロパティ.
        /// 読み込み完了した場合は null を設定する.
        /// </summary>
        public FileLoadingProgress LoadingProgress
        {
            set
            {
                this._loadingProgress = value;
                OnPropertyChanged(nameof(LoadingProgress));
                OnPropertyChanged(nameof(TitleLabel));
            }
        }

        /// <summary>
        /// 現在ファイルを読み込み中かどうか.
        /// </summary>
        public bool IsLoading
        { get { return (this._loadingProgress != null); } }

        /// <summary>
        /// ウィンドウタイトルに表示するラベル
        /// </summary>
        public string TitleLabel
        {
            get
            {
                if (this._loadingProgress != null)
                { return $"Loaded { this._loadingProgress.LastLoaded } ({ this._loadingProgress.Current } of { this._loadingProgress.Files })"; }
                else
                {
                    if (this._img1 == null)
                    { return "Simplest Image Viewer"; }
                    else
                    {
                        var controller = ((App)App.Current).Controller;
                        var sb = new StringBuilder();

                        // ファイル名
                        sb.Append(this.PageListItems[this.PageIndex].Text);

                        // 表示倍率
                        sb.Append($" ({this._zoomList[this._zoomIndex]} %)");

                        return sb.ToString();
                    }
                }
            }
        }

        #endregion

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public MainWindowViewModel()
        {
            InitCommands();

            this.Logger = LoggerFactory.CreateLogger(LoggerName.NullLogger);
            this.Images = new List<OpenedImage>();
            this._pageListItems = new List<PageListItem>();
            this._pageIndex = -1;
            this._img1 = null;
            this._img2 = null;

            // 初期状態でコンボボックスのインデックスを100%にしておく
            var zooms = AppController.ZoomList;
            for (var index = 0; index < zooms.Length; index++)
            {
                if (zooms[index] == 100)
                { this._zoomIndex = index; }
            }
        }
    }
}
