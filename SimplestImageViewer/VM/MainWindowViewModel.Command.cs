﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using clUtils.gui;
using SimplestImageViewer.Controllers;
using SimplestImageViewer.Windows;

namespace SimplestImageViewer.VM
{
    public partial class MainWindowViewModel
    {
        #region Commands

        public RelayCommand PrevImageCommand { get; set; }
        public RelayCommand NextImageCommand { get; set; }
        public RelayCommand FirstImageCommand { get; set; }
        public RelayCommand LastImageCommand { get; set; }

        public RelayCommand ZoomInCommand { get; set; }
        public RelayCommand ZoomOutCommand { get; set; }

        public RelayCommand SinglePageCommand { get; set; }
        public RelayCommand SpreadLtRCommand { get; set; }
        public RelayCommand SpreadRtLCommand { get; set; }

        public RelayCommand SettingsCommand { get; set; }
        public RelayCommand AboutCommand { get; set; }

        #endregion

        private bool CanPrevImageCommand(object sender)
        {
            return (0 < this.PageIndex);
        }

        private bool CanNextImageCommand(object sender)
        {
            return (
                this.PageListItems.Count == 0 ? false :
                this.PageIndex < this.PageListItems.Count - 1);
        }

        private void ShowPrev(object sender)
        {
            // コマンド実行不可の場合は何もしない
            if (!CanPrevImageCommand(sender))
            { return; }

            this.PageIndex--;
        }

        private void ShowNext(object sender)
        {
            // コマンド実行不可の場合は何もしない
            if (!CanNextImageCommand(sender))
            { return; }

            this.PageIndex++;
        }

        private void ShowFirst(object sender)
        {
            // コマンド実行不可の場合は何もしない
            if (!CanPrevImageCommand(sender))
            { return; }

            this.PageIndex = 0;
        }

        private void ShowLast(object sender) 
        {
            // コマンド実行不可の場合は何もしない
            if (!CanNextImageCommand(sender))
            { return; }

            this.PageIndex = this.PageListItems.Count - 1;
        }

        private bool CanZoomInCommand(object sender)
        {
            return (this._zoomIndex < this._zoomList.Length - 1);
        }

        private bool CanZoomOutCommand(object sender)
        {
            return (0 < this._zoomIndex);
        }

        private void ZoomIn(object sender)
        {
            this.ZoomIndex++;
        }

        private void ZoomOut(object sender)
        {
            this.ZoomIndex--;
        }

        private bool CanSpreadCommand(object sender)
        {
            // 複数画像の表示中のみ表示可能
            var controller = ((App)App.Current).Controller;
            return (2 < this.PageListItems.Count);
        }

        private void ShowSingle(object sender)
        { this.PageSpread = PageSpreadEnum.Single; }

        private void ShowLtR(object sender)
        { this.PageSpread = PageSpreadEnum.SpreadLtR; }

        private void ShowRtL(object sender)
        { this.PageSpread = PageSpreadEnum.SpreadRtL; }

        /// <summary>
        /// 設定画面を表示する
        /// </summary>
        /// <param name="sender"></param>
        private void ShowSettings(object sender)
        {
            // 現在の設定を複製して設定画面のDataContextに渡す
            var ctl = ((App)App.Current).Controller;
            var vm = new SettingWindowViewModel() { Config = ctl.Config.Clone() };
            var dlg = new SettingsWindow() { DataContext = vm, };

            var result = dlg.ShowDialog();
            if (result.HasValue && result.Value)
            {
                // 設定画面でOKされた場合は設定情報を保存
                var newConfig = ((SettingWindowViewModel)dlg.DataContext).Config;
                ctl.Config = newConfig;
                ctl.SaveConfig(newConfig);

                // この時点でメニューバーの自動折り畳みをオフにしている場合はウィンドウのメニューバーを明示的に表示する
                if(!newConfig.MenuAutoCollapse)
                { this.Owner.MainMenu.Visibility = System.Windows.Visibility.Visible; }
            }
        }

        /// <summary>
        /// バージョン情報画面を表示する
        /// </summary>
        /// <param name="sender"></param>
        private void ShowAbout(object sender)
        {
            (new AboutWindow()).ShowDialog();
        }

        /// <summary>
        /// コマンドの初期化
        /// </summary>
        private void InitCommands()
        {
            this.PrevImageCommand = new RelayCommand(CanPrevImageCommand, ShowPrev);
            this.NextImageCommand = new RelayCommand(CanNextImageCommand, ShowNext);
            this.FirstImageCommand = new RelayCommand(CanPrevImageCommand, ShowFirst);
            this.LastImageCommand = new RelayCommand(CanNextImageCommand, ShowLast);

            this.ZoomInCommand = new RelayCommand(CanZoomInCommand, ZoomIn);
            this.ZoomOutCommand = new RelayCommand(CanZoomOutCommand, ZoomOut);

            this.SinglePageCommand = new RelayCommand(x => true, ShowSingle);
            this.SpreadLtRCommand = new RelayCommand(x => true, ShowLtR);
            this.SpreadRtLCommand = new RelayCommand(x => true, ShowRtL);

            this.SettingsCommand = new RelayCommand(x => true, ShowSettings);
            this.AboutCommand = new RelayCommand(x => true, ShowAbout);
        }
    }
}
