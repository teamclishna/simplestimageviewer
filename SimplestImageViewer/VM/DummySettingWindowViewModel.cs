﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimplestImageViewer.VM
{
    public class DummySettingWindowViewModel : SettingWindowViewModel
    {
        public DummySettingWindowViewModel() : base()
        {
            this.Config = new AppConfig();
        }
    }
}
