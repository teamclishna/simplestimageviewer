﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using clUtils.gui;

namespace SimplestImageViewer.VM
{
    /// <summary>
    /// 設定画面のViewModel
    /// </summary>
    public class SettingWindowViewModel : NotifiableViewModel
    {
        #region Fields

        private AppConfig _config;

        #endregion

        /// <summary>
        /// アプリケーションの設定情報
        /// </summary>
        public AppConfig Config {
            get { return this._config; }
            set
            {
                this._config = value;
            }
        }

        /// <summary>
        /// ページ遷移の方法 (クリック位置)
        /// </summary>
        public bool PageTransitionBySide
        {
            get { return (this._config.TransitionMethod == PageTransitionMethod.Side); }
            set
            {
                if(value)
                {
                    this._config.TransitionMethod = PageTransitionMethod.Side;
                    OnPropertyChanged(nameof(PageTransitionBySide));
                    OnPropertyChanged(nameof(PageTransitionByButton));
                }
            }
        }

        /// <summary>
        /// ページ遷移の方法 (マウスボタン)
        /// </summary>
        public bool PageTransitionByButton
        {
            get { return (this._config.TransitionMethod == PageTransitionMethod.Button); }
            set
            {
                if (value)
                {
                    this._config.TransitionMethod = PageTransitionMethod.Button;
                    OnPropertyChanged(nameof(PageTransitionBySide));
                    OnPropertyChanged(nameof(PageTransitionByButton));
                }
            }
        }

        /// <summary>
        /// ページ送り方向 (単一画像・左側が次ページ)
        /// </summary>
        public bool PageSingleLeft
        {
            get { return (this._config.TransitionSideSingle == PageTransitionSide.Left); }
            set
            {
                if (value)
                {
                    this._config.TransitionSideSingle = PageTransitionSide.Left;
                    OnPropertyChanged(nameof(PageSingleLeft));
                    OnPropertyChanged(nameof(PageSingleRight));
                }
            }
        }

        /// <summary>
        /// ページ送り方向 (単一画像・右側が次ページ)
        /// </summary>
        public bool PageSingleRight
        {
            get { return (this._config.TransitionSideSingle == PageTransitionSide.Right); }
            set
            {
                if (value)
                {
                    this._config.TransitionSideSingle = PageTransitionSide.Right;
                    OnPropertyChanged(nameof(PageSingleRight));
                    OnPropertyChanged(nameof(PageSingleLeft));
                }
            }
        }

        /// <summary>
        /// ページ送り方向 (左綴じ・左側が次ページ)
        /// </summary>
        public bool PageLtRLeft
        {
            get { return (this._config.TransitionSideLtR == PageTransitionSide.Left); }
            set
            {
                if (value)
                {
                    this._config.TransitionSideLtR = PageTransitionSide.Left;
                    OnPropertyChanged(nameof(PageLtRLeft));
                    OnPropertyChanged(nameof(PageLtRRight));
                }
            }
        }

        /// <summary>
        /// ページ送り方向 (左綴じ・右側が次ページ)
        /// </summary>
        public bool PageLtRRight
        {
            get { return (this._config.TransitionSideLtR == PageTransitionSide.Right); }
            set
            {
                if (value)
                {
                    this._config.TransitionSideLtR = PageTransitionSide.Right;
                    OnPropertyChanged(nameof(PageLtRLeft));
                    OnPropertyChanged(nameof(PageLtRRight));
                }
            }
        }

        /// <summary>
        /// ページ送り方向 (右綴じ・左側が次ページ)
        /// </summary>
        public bool PageRtLLeft
        {
            get { return (this._config.TransitionSideRtL == PageTransitionSide.Left); }
            set
            {
                if (value)
                {
                    this._config.TransitionSideRtL = PageTransitionSide.Left;
                    OnPropertyChanged(nameof(PageRtLLeft));
                    OnPropertyChanged(nameof(PageRtLRight));
                }
            }
        }

        /// <summary>
        /// ページ送り方向 (右綴じ・右側が次ページ)
        /// </summary>
        public bool PageRtLRight
        {
            get { return (this._config.TransitionSideRtL == PageTransitionSide.Right); }
            set
            {
                if (value)
                {
                    this._config.TransitionSideRtL = PageTransitionSide.Right;
                    OnPropertyChanged(nameof(PageRtLLeft));
                    OnPropertyChanged(nameof(PageRtLRight));
                }
            }
        }

        /// <summary>
        /// フォルダを開いた場合に見開きモードを切り替えるか？
        /// </summary>
        public bool SetSpreadModeWhenFolder
        {
            get { return this._config.SetSpreadWhenFolder; }
            set
            {
                this._config.SetSpreadWhenFolder = value;
                OnPropertyChanged(nameof(SetSpreadModeWhenFolder));
            }
        }

        /// <summary>
        /// フォルダを開いたときの見開きモード (単一ページ)
        /// </summary>
        public bool FolderSpreadModeSingle
        {
            get { return this._config.PageSpreadWhenFolder == Controllers.PageSpreadEnum.Single; }
            set
            {
                if(value)
                {
                    this._config.PageSpreadWhenFolder = Controllers.PageSpreadEnum.Single;
                    OnPropertyChanged(nameof(FolderSpreadModeSingle));
                    OnPropertyChanged(nameof(FolderSpreadModeLtR));
                    OnPropertyChanged(nameof(FolderSpreadModeRtL));
                }
            }
        }

        /// <summary>
        /// フォルダを開いたときの見開きモード (右開き)
        /// </summary>
        public bool FolderSpreadModeLtR
        {
            get { return this._config.PageSpreadWhenFolder == Controllers.PageSpreadEnum.SpreadLtR; }
            set
            {
                if (value)
                {
                    this._config.PageSpreadWhenFolder = Controllers.PageSpreadEnum.SpreadLtR;
                    OnPropertyChanged(nameof(FolderSpreadModeSingle));
                    OnPropertyChanged(nameof(FolderSpreadModeLtR));
                    OnPropertyChanged(nameof(FolderSpreadModeRtL));
                }
            }
        }

        /// <summary>
        /// フォルダを開いたときの見開きモード (左開き)
        /// </summary>
        public bool FolderSpreadModeRtL
        {
            get { return this._config.PageSpreadWhenFolder == Controllers.PageSpreadEnum.SpreadRtL; }
            set
            {
                if (value)
                {
                    this._config.PageSpreadWhenFolder = Controllers.PageSpreadEnum.SpreadRtL;
                    OnPropertyChanged(nameof(FolderSpreadModeSingle));
                    OnPropertyChanged(nameof(FolderSpreadModeLtR));
                    OnPropertyChanged(nameof(FolderSpreadModeRtL));
                }
            }
        }

        /// <summary>
        /// 見開き表示時の垂直揃え方向 (上揃え)
        /// </summary>
        public bool SpreadPageAlignTop
        {
            get { return (this._config.VerticalAlign == SpreadPageAlign.Top); }
            set
            {
                if (value)
                {
                    this._config.VerticalAlign = SpreadPageAlign.Top;
                    OnPropertyChanged(nameof(SpreadPageAlignTop));
                    OnPropertyChanged(nameof(SpreadPageAlignCenter));
                    OnPropertyChanged(nameof(SpreadPageAlignBottom));
                }
            }
        }

        /// <summary>
        /// 見開き表示時の垂直揃え方向 (中央揃え)
        /// </summary>
        public bool SpreadPageAlignCenter
        {
            get { return (this._config.VerticalAlign == SpreadPageAlign.Center); }
            set
            {
                if (value)
                {
                    this._config.VerticalAlign = SpreadPageAlign.Center;
                    OnPropertyChanged(nameof(SpreadPageAlignTop));
                    OnPropertyChanged(nameof(SpreadPageAlignCenter));
                    OnPropertyChanged(nameof(SpreadPageAlignBottom));
                }
            }
        }

        /// <summary>
        /// 見開き表示時の垂直揃え方向 (下揃え)
        /// </summary>
        public bool SpreadPageAlignBottom
        {
            get { return (this._config.VerticalAlign == SpreadPageAlign.Bottom); }
            set
            {
                if (value)
                {
                    this._config.VerticalAlign = SpreadPageAlign.Bottom;
                    OnPropertyChanged(nameof(SpreadPageAlignTop));
                    OnPropertyChanged(nameof(SpreadPageAlignCenter));
                    OnPropertyChanged(nameof(SpreadPageAlignBottom));
                }
            }
        }

        /// <summary>
        /// 画像拡大率の変更方法 (画面上下端クリック)
        /// </summary>
        public bool ZoomMethodSide
        {
            get { return (this._config.ZoomMethod == ImageZoomMethod.Side); }
            set
            {
                if(value)
                {
                    this._config.ZoomMethod = ImageZoomMethod.Side;
                    OnPropertyChanged(nameof(ZoomMethodSide));
                    OnPropertyChanged(nameof(ZoomMethodWheel));
                }
            }
        }

        /// <summary>
        /// 画像拡大率の変更方法 (マウスホイール)
        /// </summary>
        public bool ZoomMethodWheel
        {
            get { return (this._config.ZoomMethod == ImageZoomMethod.MouseWheel); }
            set
            {
                if (value)
                {
                    this._config.ZoomMethod = ImageZoomMethod.MouseWheel;
                    OnPropertyChanged(nameof(ZoomMethodSide));
                    OnPropertyChanged(nameof(ZoomMethodWheel));
                }
            }
        }

        /// <summary>
        /// フォルダを開いたときにすべての画像を読み込む
        /// </summary>
        public bool LoadAllPages
        {
            get { return this._config.LoadAllPages; }
            set
            {
                this._config.LoadAllPages = value;
                OnPropertyChanged(nameof(LoadAllPages));
            }
        }

        /// <summary>
        /// 見開き表示時に最初のページを単一画像にする
        /// </summary>
        public bool FirstPageForceSingle
        {
            get { return this._config.FirstPageForceSingle; }
            set
            {
                this._config.FirstPageForceSingle = value;
                OnPropertyChanged(nameof(FirstPageForceSingle));
            }
        }

        /// <summary>
        /// 画面拡大縮小方向を反転する
        /// </summary>
        public bool InvertZoom
        {
            get { return this._config.InvertZoomIn; }
            set
            {
                this._config.InvertZoomIn = value;
                OnPropertyChanged(nameof(InvertZoom));
            }
        }

        /// <summary>
        /// メニューバーを自動的に折り畳む
        /// </summary>
        public bool MenubarAutoCollapse
        {
            get { return this._config.MenuAutoCollapse; }
            set
            {
                this._config.MenuAutoCollapse = value;
                OnPropertyChanged(nameof(MenubarAutoCollapse));
            }
        }
    }
}
