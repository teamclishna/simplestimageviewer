﻿Left Arrow: Image Transition (depend page spread settings)
Right Arrow: Image Transition (depend page spread settings)
Enter: Next Image
Backspace: Previous Image
S: Change page spread mode to Single-View
L: Change page spread mode to Left to Right
R: Change page spread mode to Right to Left
Page Up: Zoom Out
Page Down: Zoom In
