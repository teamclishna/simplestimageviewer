﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SimplestImageViewer.Windows
{
    /// <summary>
    /// AboutWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class AboutWindow : Window
    {
        /// <summary>キーバインドを記載したテキストファイルのリソース名</summary>
        private static readonly string KEYBINDING_FILE_NAME = "SimplestImageViewer.Resources.keybinding.txt";

        public AboutWindow()
        {
            InitializeComponent();

            // バージョン情報を取得してラベルに表示する
            var fi = System.Diagnostics.FileVersionInfo.GetVersionInfo(typeof(App).Assembly.Location);
            this.AuthorNameLabel.Content = fi.CompanyName;
            this.VersionText.Text = fi.ProductVersion;

            // キーバインディングをテキストファイルから読み込んで表示する
            var asm = Assembly.GetExecutingAssembly();
            using (var sr = new StreamReader(asm.GetManifestResourceStream(KEYBINDING_FILE_NAME)))
            {
                var text = sr.ReadToEnd();
                this.KeyBindingText.Text = text;
            }
        }

        private void OkButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
