﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

using SimplestImageViewer.Utils;
using SimplestImageViewer.VM;

namespace SimplestImageViewer.Windows
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// メニューバーの高さ
        /// </summary>
        private double MenuHeight;

        /// <summary>マウスボタンを押している最中はTrue</summary>
        private bool IsDragging;

        /// <summary>マウスボタンクリック時のマウスカーソルの位置</summary>
        private Point ClickStartPos;

        /// <summary>ドラッグ開始時のScrollViewerの垂直位置</summary>
        private double StartVerticalOffset;
        /// <summary>ドラッグ開始時のScrollViewerの水平位置</summary>
        private double StartHorizontalOffset;

        /// <summary>
        /// ウィンドウ長押しを実現するためのタイマー
        /// </summary>
        private DispatcherTimer LongTapTimer;

        public MainWindow()
        {
            InitializeComponent();

            var controller = ((App)App.Current).Controller;

            this.DataContext = new MainWindowViewModel()
            {
                Logger = controller.Logger,
                Owner = this,
                ZoomList = Controllers.AppController.ZoomList,
            };

            this.IsDragging = false;

            // 長押しタイマを初期化しておく
            this.LongTapTimer = new DispatcherTimer()
            {
                Interval = new TimeSpan(0, 0, 1),
                IsEnabled = false,
            };
            this.LongTapTimer.Tick += LongTapTimer_Tick;
        }

        private void LongTapTimer_Tick(object sender, EventArgs e)
        {
            // メニューパネルを表示する
            this.MainMenu.Visibility = Visibility.Visible;
            this.LongTapTimer.IsEnabled = false;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            var controller = ((App)App.Current).Controller;

            // メニューバーの高さを取得し、デフォルトで折り畳み状態にしておく
            this.MenuHeight = this.MainMenu.ActualHeight;
            if (controller.Config.MenuAutoCollapse)
            { this.MainMenu.Visibility = Visibility.Collapsed; }

            // 起動時にコマンドライン引数が指定されていればこの時点でその画像の表示を試みる
            if (0 < controller.LaunchArgs.Length)
            {
                if (ImagingUtils.IsLoadablePath(controller.LaunchArgs))
                { controller.OpenFileAsync(this, controller.LaunchArgs[0]); }
            }
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            var ctl = ((App)App.Current).Controller;
            e.Handled = ctl.KeyAction(e.Key, this);
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            // メニュー表示中の場合は何もしない
            // ここは何もせずに後続で発生するメニュー内のUI要素のイベントを実行する
            if (this.MainMenu.Visibility == Visibility.Visible)
            {
                var p = e.GetPosition(this.MainMenu);
                if (VisualTreeHelper.HitTest(this.MainMenu, p) != null)
                { return; }
            }

            // ドラッグでスクロールするためにこの時点でのマウスカーソルとスクロールバーの位置を保存しておく
            this.ClickStartPos = e.GetPosition(this);
            this.StartVerticalOffset = this.MainViewer.VerticalOffset;
            this.StartHorizontalOffset = this.MainViewer.HorizontalOffset;

            // 長押しタイマをオン
            this.LongTapTimer.IsEnabled = true;

            // ウィンドウの外までドラッグしても追従できるようにキャプチャを行う
            this.IsDragging = true;
            ((UIElement)sender).CaptureMouse();
        }

        private void Window_MouseUp(object sender, MouseButtonEventArgs e)
        {
            var currentPos = e.GetPosition(this);
            var distance = currentPos - this.ClickStartPos;

            // 長押しタイマを解除
            this.LongTapTimer.IsEnabled = false;

            // マウスボタンオン→オフまでの距離が一定以下ならクリックとみなす
            if (distance.Length < 8.0)
            {
                // 押したボタン
                var clickedButton = (e.ChangedButton == MouseButton.Left ? Controllers.MouseEventEnum.ClickLeftButton : Controllers.MouseEventEnum.ClickRightButton);

                // 押した位置
                var xPos = currentPos.X / this.Width;
                var yPos = currentPos.Y / this.Height;
                var clickedPosition = (
                    xPos <= 0.25 ? Controllers.MouseEventEnum.ClickLeftSide :
                    0.75 <= xPos ? Controllers.MouseEventEnum.ClickRightSide :
                    yPos <= 0.25 ? Controllers.MouseEventEnum.ClickUpSide :
                    0.75 <= yPos ? Controllers.MouseEventEnum.ClickDownSide :
                    Controllers.MouseEventEnum.ClickCenter
                    );

                var ctl = ((App)App.Current).Controller;
                ctl.MouseAction(clickedButton | clickedPosition, this);
            }

            // マウスドラッグのキャプチャを解除する
            ((UIElement)sender).ReleaseMouseCapture();
            this.IsDragging = false;
        }

        private void Window_MouseMove(object sender, MouseEventArgs e)
        {
            if (this.IsDragging)
            {
                var currentPos = e.GetPosition(this);
                var distance = currentPos - this.ClickStartPos;

                // マウスボタン押下位置から一定以上動いた場合は長押しタイマを解除
                if (8.0 < distance.Length)
                { this.LongTapTimer.IsEnabled = false; }

                this.MainViewer.ScrollToHorizontalOffset(this.StartHorizontalOffset - distance.X);
                this.MainViewer.ScrollToVerticalOffset(this.StartVerticalOffset - distance.Y);
            }
            else
            {
                var controller = ((App)App.Current).Controller;
                if (controller.Config.MenuAutoCollapse)
                {
                    var currentPos = e.GetPosition(this);
                    if (currentPos.Y < this.MenuHeight)
                    { this.MainMenu.Visibility = Visibility.Visible; }
                }
            }
        }

        private void Window_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            var controller = ((App)App.Current).Controller;
            controller.MouseAction((e.Delta < 0 ? Controllers.MouseEventEnum.WheelDown : Controllers.MouseEventEnum.WheelUp), this);

            e.Handled = true;
        }

        private void Window_DragLeave(object sender, DragEventArgs e)
        { }

        private void Window_DragEnter(object sender, DragEventArgs e)
        { }

        private void Window_Drop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                var droppedData = e.Data.GetData(DataFormats.FileDrop) as string[];
                if (droppedData != null && 0 < droppedData.Length)
                {
                    var ctl = ((App)App.Current).Controller;
                    ctl.OpenFileAsync(this, droppedData[0]);
                    return;
                }
            }
        }

        private void Window_DragOver(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                // 単一のフォルダ、または画像ファイルの場合にドロップを許可する
                // 単一のファイル、かつテキストファイル、MarkDownファイルの場合のみドロップ可能にする
                if (ImagingUtils.IsLoadablePath(e.Data))
                { e.Effects = DragDropEffects.Copy; }
                else
                { e.Effects = DragDropEffects.None; }
            }
            else
            { e.Effects = DragDropEffects.None; }
        }

        private void MainMenu_MouseLeave(object sender, MouseEventArgs e)
        {
            var controller = ((App)App.Current).Controller;
            if (controller.Config.MenuAutoCollapse)
            { this.MainMenu.Visibility = Visibility.Collapsed; }
        }

        /// <summary>
        /// ワーカースレッドからUIを更新する
        /// </summary>
        /// <param name="vm"></param>
        public void UpdateViewModel(MainWindowViewModel vm)
        {
            this.DataContext = vm;
            // ワーカースレッドでファイルをロード中の場合はウィンドウの操作全てを受け付けないようにする
            this.IsEnabled = !vm.IsLoading;
        }
    }
}
