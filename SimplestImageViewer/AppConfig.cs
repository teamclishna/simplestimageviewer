﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Threading.Tasks;

using SimplestImageViewer.Controllers;

namespace SimplestImageViewer
{
    /// <summary>
    /// ページ遷移の方法を示す列挙子
    /// </summary>
    [DataContract(Name = "PageTransitionMethod")]
    public enum PageTransitionMethod
    {
        /// <summary>
        /// 画面左右端クリックでページ遷移
        /// </summary>
        [EnumMember]
        Side,
        /// <summary>
        /// マウス左右ボタンでページ遷移
        /// </summary>
        [EnumMember]
        Button,
    }

    /// <summary>
    /// 次ページへ移動するための方向
    /// </summary>
    [DataContract(Name = "PageTransitionSide")]
    public enum PageTransitionSide
    {
        /// <summary>
        /// 左クリックで次ページ
        /// </summary>
        [EnumMember]
        Left,
        /// <summary>
        /// 右クリックで次ページ
        /// </summary>
        [EnumMember]
        Right,
    }

    /// <summary>
    /// 見開き表示時の垂直揃え
    /// </summary>
    [DataContract(Name = "SpreadPageAlign")]
    public enum SpreadPageAlign
    {
        /// <summary>
        /// 上揃え
        /// </summary>
        [EnumMember] 
        Top,
        /// <summary>
        /// 中央揃え
        /// </summary>
        [EnumMember] 
        Center,
        /// <summary>
        /// 下揃え
        /// </summary>
        [EnumMember] 
        Bottom,
    }

    /// <summary>
    /// 画像の拡大率の変更方法
    /// </summary>
    [DataContract(Name = "ImageZoomMethod")]
    public enum ImageZoomMethod
    {
        /// <summary>
        /// 画面の上下端クリック
        /// </summary>
        [EnumMember]
        Side,
        /// <summary>
        /// マウスホイール
        /// </summary>
        [EnumMember]
        MouseWheel,
    }

    /// <summary>
    /// アプリケーションの設定を格納するためのクラス
    /// </summary>
    [DataContract]
    [Serializable]
    public class AppConfig
    {
        /// <summary>ページ遷移の方法</summary>
        [DataMember]
        public PageTransitionMethod TransitionMethod { get; set; }

        /// <summary>次ページへの遷移方向 (単一ページ)</summary>
        [DataMember]
        public PageTransitionSide TransitionSideSingle { get; set; }

        /// <summary>次ページへの遷移方向 (見開き左綴じ)</summary>
        [DataMember]
        public PageTransitionSide TransitionSideLtR { get; set; }

        /// <summary>次ページへの遷移方向 (見開き右綴じ)</summary>
        [DataMember]
        public PageTransitionSide TransitionSideRtL { get; set; }

        /// <summary>見開き表示時の垂直揃え</summary>
        [DataMember]
        public SpreadPageAlign VerticalAlign { get; set; }

        /// <summary>画像の拡大率の変更方法</summary>
        [DataMember]
        public ImageZoomMethod ZoomMethod { get; set; } 

        /// <summaryフォルダを開く場合に一覧取得時点ですべての画像を読み込む</summary>
        [DataMember]
        public bool LoadAllPages { get; set; }

        /// <summary>見開き表示時に最初の画像だけは単一ページ表示にする</summary>
        [DataMember]
        public bool FirstPageForceSingle { get; set; }

        /// <summary>拡大率変更時のホイール方向を反転する</summary>
        [DataMember]
        public bool InvertZoomIn { get; set; }

        /// <summary>メニューバーを自動的に折り畳む</summary>
        [DataMember]
        public bool MenuAutoCollapse { get; set; }

        /// <summary>フォルダを開いた場合に見開きモードを設定するかどうか</summary>
        [DataMember]
        public bool SetSpreadWhenFolder { get; set; }

        /// <summary>SetSpreadWhenFolder が true、かつフォルダを開いたときに設定する表示モード</summary>
        [DataMember]
        public PageSpreadEnum PageSpreadWhenFolder { get; set; }

        /// <summary>
        /// このオブジェクトの複製を生成して返す
        /// </summary>
        /// <returns></returns>
        public AppConfig Clone()
        {
            return new AppConfig()
            {
                TransitionMethod = this.TransitionMethod,
                TransitionSideSingle = this.TransitionSideSingle,
                TransitionSideLtR = this.TransitionSideLtR,
                TransitionSideRtL = this.TransitionSideRtL,
                VerticalAlign = this.VerticalAlign,
                LoadAllPages = this.LoadAllPages,
                FirstPageForceSingle = this.FirstPageForceSingle,
                ZoomMethod = this.ZoomMethod,
                InvertZoomIn = this.InvertZoomIn,
                MenuAutoCollapse = this.MenuAutoCollapse,
                SetSpreadWhenFolder = this.SetSpreadWhenFolder,
                PageSpreadWhenFolder = this.PageSpreadWhenFolder,
            };
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public AppConfig()
        {
            this.TransitionMethod = PageTransitionMethod.Button;
            this.TransitionSideSingle = PageTransitionSide.Left;
            this.TransitionSideLtR = PageTransitionSide.Right;
            this.TransitionSideRtL = PageTransitionSide.Left;
            this.VerticalAlign = SpreadPageAlign.Top;
            this.ZoomMethod = ImageZoomMethod.MouseWheel;
            this.InvertZoomIn = false;
            this.LoadAllPages = false;
            this.FirstPageForceSingle = false;
            this.MenuAutoCollapse = true;
            this.SetSpreadWhenFolder = false;
            this.PageSpreadWhenFolder = PageSpreadEnum.Single;
        }
    }
}
