﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;

using SimplestImageViewer.Controllers;

namespace SimplestImageViewer.Utils
{
    /// <summary>
    /// 画像表示に使用するユーティリティ関数群
    /// </summary>
    public static class ImagingUtils
    {
        /// <summary>対応可能なファイル型式</summary>
        private static readonly string[] SUPPRTED_FILE_TYPES = {
            ".bmp",
            ".ico",
            ".png",
            ".jpg",
            ".jpeg",
            ".gif",
            ".tif",
            ".tiff",
        };

        /// <summary>
        /// 画像表示時のピクセル幅を計算する
        /// </summary>
        /// <param name="oi"></param>
        /// <param name="zoom"></param>
        /// <returns></returns>
        public static double CalcWidth(this OpenedImage oi, int zoom)
        { return (oi == null ? 0 : oi.OpenImage.CalcWidth(zoom)); }

        /// <summary>
        /// 画像表示時のピクセル幅を計算する
        /// </summary>
        /// <param name="img"></param>
        /// <param name="zoom"></param>
        /// <returns></returns>
        public static double CalcWidth(this BitmapImage img, int zoom)
        { return (img == null ? 0 : img.PixelWidth * zoom / 100); }

        /// <summary>
        /// 画像表示時のピクセル高さを計算する
        /// </summary>
        /// <param name="oi"></param>
        /// <param name="zoom"></param>
        /// <returns></returns>
        public static double CalcHeight(this OpenedImage oi, int zoom)
        { return (oi == null ? 0 : oi.OpenImage.CalcHeight(zoom)); }

        /// <summary>
        /// 画像表示時のピクセル高さを計算する
        /// </summary>
        /// <param name="img"></param>
        /// <param name="zoom"></param>
        /// <returns></returns>
        public static double CalcHeight(this BitmapImage img, int zoom)
        { return (img == null ? 0 : img.PixelHeight * zoom / 100); }

        /// <summary>
        /// 画面表示時の画像上端のピクセル位置を計算する
        /// </summary>
        /// <param name="img1">計算対象の画像</param>
        /// <param name="img2">見開き表示の場合のもう一方の画像</param>
        /// <param name="zoom">画像の拡大率</param>
        /// <returns></returns>
        public static double CalcTop(OpenedImage img1, OpenedImage img2, int zoom)
        {
            // 対象画像がnullの場合は計算不要
            if(img1 == null || img1.OpenImage == null)
            { return 0.0; }

            var conf = ((App)App.Current).Controller.Config;
            switch (conf.VerticalAlign)
            {
                case SpreadPageAlign.Top:
                    // 上揃えの場合は無条件で0
                    return 0.0;
                case SpreadPageAlign.Center:
                    if (img2 == null || img2.OpenImage == null)
                    { return 0.0; }
                    else if (img2.OpenImage.PixelHeight < img1.OpenImage.PixelHeight)
                    { return 0.0; }
                    else
                    { return (img2.OpenImage.PixelHeight - img1.OpenImage.PixelHeight) * zoom / 100 / 2; }
                case SpreadPageAlign.Bottom:
                    if (img2 == null || img2.OpenImage == null)
                    { return 0.0; }
                    else if (img2.OpenImage.PixelHeight < img1.OpenImage.PixelHeight)
                    { return 0.0; }
                    else
                    { return (img2.OpenImage.PixelHeight - img1.OpenImage.PixelHeight) * zoom / 100; }
                default:
                    return 0.0;
            }
        }

        /// <summary>
        /// 指定したドロップオブジェクトが単一のファイル、またはフォルダであり、
        /// かつファイルの場合は対応可能な画像形式であるかどうかをチェックする
        /// </summary>
        /// <param name="droppedObject"></param>
        /// <returns></returns>
        public static bool IsLoadablePath(object droppedObject)
        {
            var files = droppedObject as string[];

            // ファイルパス (を示す文字列の配列) 以外をドロップした場合は終了
            if (files == null)
            { return false; }

            // 単一要素でない場合は終了
            if (files.Length != 1)
            { return false; }

            // 存在するフォルダであった場合はOK
            var droppedFolder = new System.IO.DirectoryInfo(files[0]);
            if (droppedFolder.Exists)
            { return true; }

            // ファイルであった場合は存在する、かつ対応可能な画像形式である場合OK
            var droppedFile = new FileInfo(files[0]);
            if(IsImageFile(droppedFile))
            { return true; }

            return false;
        }

        /// <summary>
        /// 指定したファイルが画像ファイルかどうかを判定する
        /// </summary>
        /// <param name="fi"></param>
        /// <returns></returns>
        public static bool IsImageFile(FileInfo fi)
        { return (fi.Exists && SUPPRTED_FILE_TYPES.Any(x => x.Equals(fi.Extension.ToLower()))); }

        /// <summary>
        /// 指定した画像を開く
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static BitmapImage LoadImage(string path)
        {
            // ドロップしたファイルを開く
            var bmp = new BitmapImage();
            bmp.BeginInit();
            bmp.UriSource = new Uri(path);
            bmp.EndInit();

            return bmp;
        }
    }
}
