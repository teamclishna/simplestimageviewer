﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

using SimplestImageViewer.Controllers;
using SimplestImageViewer.Windows;

using clUtils.log;

namespace SimplestImageViewer
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public AppController Controller { get; private set; }

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            this.Controller = new AppController()
            {
                LaunchArgs = e.Args,
                Logger = InitLogger(),
            }.Init();

            new MainWindow().Show();
        }

        /// <summary>
        /// アプリケーションが出力するロガーオブジェクトの初期化処理
        /// </summary>
        /// <returns>初期化済みのロガーオブジェクト</returns>
        private ILogger InitLogger()
        {
#if DEBUG
            return new clUtils.log.ConsoleLogger()
            {
                OutputLogLevel = new clUtils.log.LogLevel[] {
                    clUtils.log.LogLevel.ERROR,
                    clUtils.log.LogLevel.WARN,
                    clUtils.log.LogLevel.INFO,
                    clUtils.log.LogLevel.DEBUG
                },
                ShowCurrentLine = false,
                ShowThreadID = true,
                ShowCallStackIndentation = true,
                IndentString = " ",
                ShowTimestamp = true,
                ShowLogLevel = true,
            };
#else
            return new clUtils.log.NullLogger();
#endif
        }
    }
}
