﻿# Simplest Image Viewer

## これは何? (What's this?)

![Icon on TaskTray](./readme_images/main.png)

Drag & Drop で画像を表示するシンプルなアプリケーションです。

単一の画像、またはフォルダをドロップしてその中の画像を表示することができます。

フォルダをドロップした場合は見開き表示することができるので
本をスキャンした画像を閲覧するのに適しています。

It is a simple application that displays images with Drag & Drop.

You can drop a single image or a folder to see the images in it.

If you drop a folder, you can display it in a two-page spread.
Suitable for viewing scanned images of books.

## 必要な動作環境 (System Requirements)

* Windows 10
* .net 6.0

## インストール (How to install)

このアプリケーションのインストーラはありません。

SimplestImageViewer.exe とフォルダ内にあるすべてのファイル、サブフォルダを同一のフォルダに置いて
SimplestImageViewer.exe を実行します。

There is no installer for this application.

Put SimplestImageViewer.exe and all files and subfolders in the folder in the same folder
Run SimplestImageViewer.exe.

または、エクスプローラのファイルの関連付け、「送る」メニューに登録することも出来ます。

この場合、SimplestImageViewer.exe のショートカットを作成し、
コマンドライン引数の 1 つ目に表示する画像ファイル、またはフォルダのフルパスが渡されるように設定してください。

Alternatively, you can associate files in Explorer and register them in the "Send" menu.

In this case, create a shortcut for SimplestImageViewer.exe and
Set the full path of the image file or folder to be displayed as the first command line argument to be passed.

## アンインストール (How to uninstall)

このアプリケーションのアンインストーラはありません。
インストール手順で説明したフォルダを削除します。

This application does not use an uninstaller.
When deleting an application, delete the files described in the installation procedure.

以下のフォルダにアプリケーションの設定ファイルが保存されます。
設定を残す必要がないのならこのフォルダとサブフォルダを削除します。

If you do not need to leave the application settings, delete this folder and its subfolders.

``%USERPROFILE%\Roaming\TeamClishnA\SimplestImageViewer``

## 使い方 (How to use)

アプリケーションを起動すると空白のウィンドウが表示されます。

A blank window will appear when you launch the application.

ウィンドウに画像ファイル、またはフォルダを表示すると画像が表示されます。

When you display an image file or folder in the window, the image is displayed.

画像の表示中にマウスホイールを回転させると画像の拡大率を変更できます。

You can change the magnification of the image by rotating the mouse wheel while the image is displayed.

フォルダをドロップした場合、マウスクリックで画像を順に表示します。

If you drop a folder, click the mouse to display the images in order.


## アプリケーションの設定 (Application Settings)

![Icon on TaskTray](./readme_images/settings.png)

### Page Transition Method

フォルダを開いた場合に画像を切り替える方法を設定します

#### Click right or left side of window

ウィンドウの左右端をクリックすることで画像を切り替えます。
「左右端」とはウィンドウの左右端からウィンドウの幅の 25% よりも外側です。

You can switch images by clicking the left and right edges of the window.

The "left and right edges" are outside the left and right edges of the window,
more than 25% of the width of the window.

#### Click left or right mouse button

マウスの左右ボタンをクリックすることで画像を切り替えます。

You can switch images by clicking the left and right mouse buttons.

画面の表示モード (単一画像、左綴じ、右綴じ) のそれぞれについて、
左右のクリックに対してどちらを「次の画像」として扱うかを設定できます。

"Click right or left side of window" を設定している場合は "Spread Left to Right" のみを "Right to forward" に設定し、
"Click left or right mouse button" を設定している場合は全てのモードで "Left to forward" を設定することを推奨します。

For each of the screen display modes (single image, left binding, right binding)
You can set which is treated as the "next image" for left and right clicks.

If "Click right or left side of window" is set, only "Spread Left to Right" is set to "Right to forward"
If you have set "Click left or right mouse button", it is recommended to set "Left to forward" in all modes.

#### Left to forward

画面の左端、またはマウスの左クリックで「次の画像」を表示します。

Display the "next image" by left-clicking on the left edge of the screen or the mouse.

#### Right to forward

画面の右端、またはマウスの左クリックで「次の画像」を表示します。

Display the "next image" by clicking the right edge of the screen or left-clicking the mouse.

### Page Spread Mode

フォルダをドロップした場合に見開き表示モードを自動的に切り替えるかどうかをチェックボックスで設定します。
また、チェックボックスをオンにした場合、自動的に切り替える表示モードを設定します。

Set check box to change view mode automatically when dropped folder.
Also, when the check box is selected, select radio button to set display mode to switch display mode.

### Image ZoomIn/Out Medhod

画像の拡大率を変更する方法を設定します。

Set how to change the magnification of the image.

#### Click up or down side of window

画面の上下端をクリックすることで拡大率を変更します。
「上下端」とはウィンドウの上下端からウィンドウの高さの 25% よりも外側です。

Click the top and bottom edges of the screen to change the magnification.
The "top and bottom" is outside the top and bottom of the window, more than 25% of the window height.

#### Click mouse wheel

マウスホイールを回転させることで拡大率を変更します。

Change the magnification by rotating the mouse wheel.

### Invert zoom in/out direction

画像の拡大、縮小操作を反転します。

Inverts the image enlargement / reduction operation.

### Vertical Alignment

画像を見開き表示する場合に左右の画像の配置を設定します。

Set the arrangement of the left and right images when displaying images in a two-page spread.

### Load all images when drop folder

フォルダを開いたときにすべての画像を読み込みます。
チェックをオフにすると画像を切り替えたときに読み込みます。

Loads all images when you open the folder.
If the check is unchecked, it will be loaded when the image is switched.

ファイルサーバから読み込む場合など、大量の画像を最初に読み込むとフリーズしたように見える場合にはチェックをオフにしてください。

Uncheck it if it looks like it freezes when you first load a large number of images,
such as when loading from a file server.

### Show single image of first page when page-spread mode

フォルダの画像を見開き表示する場合に、最初の画像だけは2ページ表示しません。
書籍をスキャンした画像を表示する場合に最初の画像が表紙である場合に有効です。

When displaying the images in a folder in a two-page spread, only the first image is not displayed on two pages.
This is useful when the first image is the cover page when displaying a scanned image of a book.

### Collapse menu bar automatically

ウィンドウ上部のメニューバーを非表示にします。

Hides the menu bar at the top of the window.

メニューバー非表示の場合、マウスカーソルをウィンドウ上部に移動させる、またはウィンドウ上で
左クリックを長押しするとメニューバーを表示します。

When the menu bar is hidden, move the mouse cursor to the top of the window or on the window
Press and hold the left click to display the menu bar.

## ライセンス (License)

### Simplest Image Viewer について (About S.I.V.)

このアプリケーションはシェアウェアです。
アプリケーションを使用するための料金は株式会社ベクター様の「シェアレジ」で支払う事が出来ます。

料金は 500円 です。実際に支払う料金はシェアレジ手数料 (100円) と消費税が加算され、660円です。
料金を支払わないことによる機能の制限はなく、また料金を支払うことによる機能の追加はありません。
また、料金を支払った場合に特に必要な手続きはありません。継続して使用することができます。

Team ClishnA のシェアウェアに関する FAQ は以下の URL を参照してください。

This application is shareware.
The fee for using the application can be paid at "Share Registration" of Vector Co., Ltd.

The fee is 500 JPY. The actual fee to be paid is 660 JPY,
including the share registration fee (100 JPY) and consumption tax.

There are no functional restrictions due to not paying, and no additional features due to paying.
In addition, there is no special procedure required when paying the fee. You can continue to use it.

For FAQs on Team ClishnA shareware, please refer to the following URL.

* [FAQ](https://clishna.info/apps/shareware/)

このアプリケーションはソースコードを公開しています。
このソースコードをダウンロードしてあなたはバグを修正したり自分に必要な機能を追加することができます。

このアプリケーションはオープンソースソフトウェア**ではありません**。

あなたはこのアプリケーションを配布、転載することはできません。
あなたが修正したソースコードを配布する場合、修正箇所のみを配布することができます。

This application has released the source code.
You can download the source code and fix bugs or add functions.

This application is **not** open source software.

You cannot distribute or reprint this application.
When you distribute the source code that you modified,
you can distribute only the modified part.

## 作者 (Author)

Team ClishnA

* [InfoClishnA](https://clishna.info/apps/simplestimageviewer/)
* [Source code](https://bitbucket.org/teamclishna/simplestimageviewer/)

